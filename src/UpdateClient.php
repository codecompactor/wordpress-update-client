<?php

namespace CodeCompactor\WordPressUpdateClient;


final class UpdateClient
{

    private $plugin_file, $cc_update_url;

    private function __construct($plugin_file, $cc_access_token)
    {
        $this->plugin_file = $plugin_file;
        $this->cc_update_url = "https://codecompactor.com/api/repository/{$cc_access_token}/updates.json";

        //Exclude from WP updates
        add_filter(
            'http_request_args',
            [$this, 'filter_http_requests'],
            5,
            2
        );

        //Override requests for plugin information
        add_filter(
            'plugins_api',
            [$this, 'interceptPluginApiRequest'],
            20,
            3
        );

        // check for update twice a day (same schedule as normal WP plugins)
        add_action('plugins_loaded', function() use ($cc_access_token) {

            // If the scheduled event is not set, set it
            if (!wp_next_scheduled("cc_check_for_{$cc_access_token}_update")) {
                wp_schedule_event(time(), 'twicedaily', "cc_check_for_{$cc_access_token}_update");
            }

            // If the 'legacy' version is still scheduled, unschedule it
            if ($nextRun = wp_next_scheduled('cc_check_for_update')) {
                wp_unschedule_event($nextRun, 'cc_check_for_update');
            }

        });

        add_action(
            "cc_check_for_{$cc_access_token}_update",
            [$this, 'check_for_update']
        );

        //remove cron task upon deactivation
        register_deactivation_hook(
            $plugin_file,
            [$this, 'deactivation_hook']
        );

        // hook for if we are manually checking for an update
        add_action('admin_init', function() use ($cc_access_token) {
            if (
                array_key_exists("cc_check_for_{$cc_access_token}_update", $_GET) &&
                check_admin_referer("cc_check_for_{$cc_access_token}_update") &&
                current_user_can('update_plugins')
            ) {
                $this->check_for_update();
            }
        });

        // Add the 'check for update' link to the plugin action links
        add_filter(
            'plugin_action_links_' . plugin_basename($plugin_file),
            function (array $links) use ($cc_access_token)
            {
                $linkUrl = wp_nonce_url(
                    add_query_arg(
                        [
                            "cc_check_for_{$cc_access_token}_update" => 1
                        ],
                        self_admin_url('plugins.php')
                    ),
                    "cc_check_for_{$cc_access_token}_update"
                );
                return array_merge(
                    $links,
                    [
                        "<a href='{$linkUrl}'>Check For Updates</a>"
                    ]
                );
            }
        );
    }

    /**
     * Invokes the update checker.
     * Returns true on success, false on failure.
     *
     * @param $plugin_file
     * @param $cc_access_token
     * @return bool
     */
    public static function invokeUpdateChecker($plugin_file, $cc_access_token)
    {
        static $hasBeenInvoked = false;

        if (!$hasBeenInvoked) {
            new static($plugin_file, $cc_access_token);
        }

        $hasBeenInvoked = true;

        return $hasBeenInvoked;
    }

    /**
     * Checks for an update.
     *
     * @return bool
     */
    public function check_for_update() {
        $plugin_folder = plugin_basename(dirname($this->plugin_file));
        if (defined('WP_INSTALLING')) {
            return false;
        }

        $response = json_decode(wp_remote_get($this->cc_update_url)['body']);
        $version = $response->version;
        if ($this->plugin_info("Version") == $version) {
            return false;
        }
        $plugin_transient = get_site_transient('update_plugins');
        $plugin_transient->response[$plugin_folder . '/' . basename($this->plugin_file)] = (object)[
            'name' => $response->name,
            'sections' => $response->sections,
            'slug'        => $plugin_folder,
            'new_version' => $version,
            'url'         => $this->plugin_info("AuthorURI"),
            'package'     => $response->download_url
        ];
        set_site_transient('update_plugins', $plugin_transient);
        return true;
    }

    /**
     * On deactivation, clear out the cron job.
     */
    public function deactivation_hook() {
        wp_clear_scheduled_hook('cc_check_for_update');
    }

    /**
     * Removes this plugin from the update check.
     *
     * @param $r
     * @param $url
     * @return mixed
     */
    public function filter_http_requests($r, $url) {
        // Check to see if this is a plugin update request
        if (0 !== strpos($url, 'api.wordpress.org/plugins/update-check')) {
            return $r;
        }
        $plugins = unserialize($r['body']['plugins']);
        unset($plugins->plugins[plugin_basename(__FILE__)]);
        unset($plugins->active[array_search(plugin_basename(__FILE__), $plugins->active)]);
        $r['body']['plugins'] = serialize($plugins);
        return $r;
    }

    private function get_transient() {
        return get_site_transient('update_plugins')->response[plugin_basename(dirname($this->plugin_file)).'/'.basename($this->plugin_file)];
    }

    private function plugin_info($i) {
        if (!function_exists('get_plugins')) {
            require_once(ABSPATH . 'wp-admin/includes/plugin.php');
        }
        $plugin_folder = get_plugins('/' . plugin_basename(dirname(basename($this->plugin_file))));
        return $plugin_folder[plugin_basename($this->plugin_file)][$i];
    }

    /**
     * Intercept plugins_api() calls that request information about our plugin and
     * use the configured API endpoint to satisfy them.
     *
     * @see plugins_api()
     *
     * @param mixed $result
     * @param string $action
     * @param array|object $args
     * @return mixed
     */
    public function interceptPluginApiRequest($result, $action = null, $args = null)
    {
        $relevant = (
            ($action == 'plugin_information') &&
            isset($args->slug)
            &&
            (
                ($args->slug == dirname($this->plugin_file)) ||
                ($args->slug == plugin_basename(dirname($this->plugin_file)))
            )
        );
        if (!$relevant) {
            return $result;
        }
        return (object) [
            'name' => $this->get_transient()->name,
            'slug' => $args->slug,
            'version' => $this->get_transient()->new_version,
            'author' => $this->plugin_info("AuthorURI"),
            'sections' => (array) $this->get_transient()->sections,
            'download_link' => $this->get_transient()->package
        ];
    }

}